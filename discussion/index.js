// console.log("message from the index.js")

// [SECTION] Introduction to Arrays

	//practice: create/declare nultiple variable 


let student1 = 'Martin';
let student2 = 'John';
let student3 = 'Maria';
let student4 = 'Ernie';
let student5 = 'Bert';

// store the following values insiee a single container.
// to declare an array => we simply use 'array literal' or square bracket '[]'
// use '=' => assignment operation to contain the structure inside the variable
// be careful when selecting variable names.
 let batch = ['Martin', 'Maria', 'John', 'Ernie','Bert'];
 console.log(batch);


//create an array that will contain different computerBrands
let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer,','Toshiba','Fujitsu']
  console.log(computerBrands);
// '' or "" => both are used to declare a string data type in JS.

//=> 'apple' === "apple"
// 'apple' //correct
// "apple" //correct
// 'apple" //wrong

//How to Choose?

//=> Use case of the data

//1. if your going to use qoutations, dialog inside a string.

//if you will use single qoutation when declaring such values like below, in will result to prematurely end the statement.
let message = "Using Java Script's Array Literal";
console.log(message);

let lastWords = '"I Shall Return!" - McArthur';
console.log(lastWords);

//NOTE: When selecting the correct qoutation type, you can use them as an [ESCAPE TOOL] to properly declare expressions within a string.

//[ALTERNATIVE APPROACH] '\' forward slash to insert qoutaions

//example:
let message2 = 'Using Java Script\'s Array Literal';
console.log(message2);

//('\') this can be use to escape both single/double qoutation expression.

//NOTE: a lot of developers preer to use '' (single qoutation) as it is easier/simpler to use when declaring string data (AGAIN, take note of the use cases.)


//practicing the use of git branches




